package fr.efrei.web.app;

import java.util.Calendar;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.services.CustomerService;


public class AppEngine {

//	@Autowired
//	private CustomerService customerService;

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("businessServices.xml");
		
		CustomerService customerService = (CustomerService) context.getBean("customerService");
		
		Customer customerTest = new Customer("TestFirstName", "TestLastName");
		customerTest.setBirthDate(Calendar.getInstance().getTime());
		customerTest.setAddressNumber(3);
		customerTest.setCity("Barcelone");
		customerTest.setCustomerSince(Calendar.getInstance().getTime());
		customerTest.setStreetAddress("Test nom rue");
		customerTest.setZipCode(0000);
		customerService.persist(customerTest);
		
		//SpringApplication.run(AppEngine.class, args);
	
	}
	
	
//	public void run(String... args) throws Exception{
//		Customer customerTest = new Customer("Simona", "Halep");
//		customerTest.setBirthDate(Calendar.getInstance().getTime());
//		customerTest.setAddressNumber(3);
//		customerTest.setCity("Barcelone");
//		customerTest.setCustomerSince(Calendar.getInstance().getTime());
//		customerTest.setStreetAddress("Test nom rue");
//		customerTest.setZipCode(0000);
//		//customerService.persist(customerTest);
//	}

}
