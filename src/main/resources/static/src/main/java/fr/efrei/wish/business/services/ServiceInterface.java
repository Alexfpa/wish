package fr.efrei.wish.business.services;

import java.util.List;


public interface ServiceInterface<Bean, Entity> {
	
	public void persist(Entity entity);
	
	public void update(Entity entity);

	public Entity findById(String id);

	public void delete(String id);

	public List<Entity> findAll();

	public void deleteAll();

	
}
