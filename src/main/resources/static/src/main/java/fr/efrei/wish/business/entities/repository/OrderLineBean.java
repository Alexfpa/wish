package fr.efrei.wish.business.entities.repository;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_line")
public class OrderLineBean implements Bean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "amount")
	private int amount;

	@Column(name = "purchase_price")
	private double purchasePrice;

	@Column(name = "product_id")
	private int productId;

	@Column(name = "order_id")
	private double orderId;

	public OrderLineBean(int amount, double purchasePrice) {
		this.amount = amount;
		this.purchasePrice = purchasePrice;
	}

	protected void initialize() {
	}
	
	public OrderLineBean() {
		this.initialize();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public double getOrderId() {
		return orderId;
	}

	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "OrderLineBean [id=" + id + ", amount=" + amount + ", purchasePrice=" + purchasePrice + ", productId="
				+ productId + ", orderId=" + orderId + "]";
	}

}
