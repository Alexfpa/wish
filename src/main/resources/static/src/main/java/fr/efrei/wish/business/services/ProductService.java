package fr.efrei.wish.business.services;

import java.util.ArrayList;
import java.util.List;

import fr.efrei.wish.business.entities.Comment;
import fr.efrei.wish.business.entities.Product;
import fr.efrei.wish.business.entities.repository.CommentBean;
import fr.efrei.wish.business.entities.repository.ProductBean;
import fr.efrei.wish.business.daos.CommentDaoImpl;
import fr.efrei.wish.business.daos.ProductDaoImpl;

public class ProductService implements ServiceInterface<ProductBean, Product> {

	private ProductDaoImpl productDao;
	private CommentDaoImpl commentDao;

	public ProductService() {
		productDao = new ProductDaoImpl();
	}

	public void persist(Product product) {
		ProductBean productBean = null;
		List<Comment> commentsList = null;
		if (product != null) {
			productBean = productDao.toDto(product);
			for (Comment comment : product.getComments()) {
				if (this.findById("" + comment.getId()) == null) {
					CommentBean commentBean = commentDao.toDto(comment);
					commentDao.openCurrentSessionwithTransaction();
					commentDao.persist(commentBean);
					commentDao.closeCurrentSessionwithTransaction();
				}
			}

		}
		productDao.openCurrentSessionwithTransaction();
		productDao.persist(productBean);
		productDao.closeCurrentSessionwithTransaction();
	}

	public void update(Product product) {
		ProductBean productBean = null;
		List<Comment> commentsList = null;

		if (product != null) {
			productBean = productDao.toDto(product);
			for (Comment comment : product.getComments()) {
				if (this.findById("" + comment.getId()) == null) {
					CommentBean commentBean = commentDao.toDto(comment);
					commentDao.openCurrentSessionwithTransaction();
					commentDao.persist(commentBean);
					commentDao.closeCurrentSessionwithTransaction();
				}
			}

		}
		productDao.openCurrentSessionwithTransaction();
		productDao.update(productBean);
		productDao.closeCurrentSessionwithTransaction();
	}

	public Product findById(String id) {
		Product res = null;

		productDao.openCurrentSession();
		ProductBean product = productDao.findById(id);
		productDao.closeCurrentSession();

		commentDao.openCurrentSession();
		List<CommentBean> comments = commentDao.findAll();
		productDao.closeCurrentSession();

		ArrayList<Comment> commentsList = new ArrayList<Comment>();
		for (CommentBean comment : comments) {
			if (comment.getProductId() == product.getId()) {
				commentsList.add(commentDao.toEntity(comment));
			}
		}

		if (product != null) {
			res = productDao.toEntity(product);
			res.setComments(commentsList);
		}

		return res;
	}

	public void delete(String id) {
		productDao.openCurrentSessionwithTransaction();
		ProductBean product = productDao.findById(id);
		productDao.delete(product);
		productDao.closeCurrentSessionwithTransaction();
	}

	public List<Product> findAll() {
		List<Product> productList = new ArrayList<Product>();

		productDao.openCurrentSession();
		List<ProductBean> products = productDao.findAll();
		productDao.closeCurrentSession();

		for (ProductBean product : products) {
			commentDao.openCurrentSession();
			List<CommentBean> comments = commentDao.findAll();
			productDao.closeCurrentSession();

			ArrayList<Comment> commentsList = new ArrayList<Comment>();
			for (CommentBean comment : comments) {
				if (comment.getProductId() == product.getId()) {
					commentsList.add(commentDao.toEntity(comment));
				}
			}

			if (product != null) {
				Product res = productDao.toEntity(product);
				res.setComments(commentsList);
				productList.add(res);
			}
		}
		return productList;
	}

	public void deleteAll() {
		productDao.openCurrentSessionwithTransaction();
		productDao.deleteAll();
		productDao.closeCurrentSessionwithTransaction();
	}

	public ProductDaoImpl getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDaoImpl productDao) {
		this.productDao = productDao;
	}

	public CommentDaoImpl getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDaoImpl commentDao) {
		this.commentDao = commentDao;
	}

}
