package fr.efrei.wish.business.daos;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.efrei.wish.business.entities.Product;
import fr.efrei.wish.business.entities.repository.ProductBean;

public class ProductDaoImpl extends AbstractEntityDao implements EntityDao<ProductBean, Product, String> {
	
	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
//		Configuration configuration = new Configuration().configure();
//		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
//				.applySettings(configuration.getProperties());
//		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//		return sessionFactory;
		
		try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();
            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (HibernateException he) {
            System.out.println("Session Factory creation failure");
            throw he;
        }
		
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(ProductBean entity) {
		getCurrentSession().save(entity);

	}

	public void update(ProductBean entity) {
		getCurrentSession().update(entity);

	}

	public ProductBean findById(String id) {
		ProductBean product = (ProductBean) getCurrentSession().get(ProductBean.class, id);
		return product;
	}

	public void delete(ProductBean entity) {
		getCurrentSession().delete(entity);
	}

	public List<ProductBean> findAll() {
		List<ProductBean> products = (List<ProductBean>) getCurrentSession().createQuery("from Customer").list();
		return products;

	}

	public void deleteAll() {
		List<ProductBean> entityList = findAll();
		for (ProductBean entity : entityList) {
			delete(entity);
		}

	}

	public Product toEntity(ProductBean bean) {
		Product product = null;

		if (bean != null) {
			product = new Product();
			product.setId(bean.getId());
			product.setInStock(bean.isInStock());
			product.setName(bean.getName());
			product.setPrice(new BigDecimal(bean.getPrice()));
			product.setComments(null);
		}
		return product;
	}

	public ProductBean toDto(Product entity) {
		ProductBean productBean = null;
		
		if(entity != null) {
			productBean = new ProductBean(entity.getName(), entity.getPrice().doubleValue(), entity.isInStock());
			productBean.setId(entity.getId());
		}
		
		return productBean;
	}
}
