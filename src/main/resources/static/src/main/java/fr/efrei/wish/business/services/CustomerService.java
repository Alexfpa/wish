package fr.efrei.wish.business.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.entities.repository.CustomerBean;
import fr.efrei.wish.business.daos.CustomerDaoImpl;

public class CustomerService implements ServiceInterface<CustomerBean, Customer> {

	private CustomerDaoImpl customerDao;

	public void persist(Customer entity) {
		if(entity != null) {
			CustomerBean customerBean = customerDao.toDto(entity);
			customerDao.openCurrentSessionwithTransaction();
			customerDao.persist(customerBean);
			customerDao.closeCurrentSessionwithTransaction();
		}
	}

	public void update(Customer entity) {
		if(entity != null) {
			CustomerBean customerBean = customerDao.toDto(entity);
			customerDao.openCurrentSessionwithTransaction();
			customerDao.update(customerBean);
			customerDao.closeCurrentSessionwithTransaction();
		}
		
	}

	public Customer findById(String id) {
		Customer customer = null;
		customerDao.openCurrentSession();
		CustomerBean customerBean = customerDao.findById(id);
		customerDao.closeCurrentSession();
		
		if(customerBean != null) {
			customer = customerDao.toEntity(customerBean);
		}
		return customer;
	}

	public void delete(String id) {
		customerDao.openCurrentSessionwithTransaction();
		CustomerBean customer = customerDao.findById(id);
		customerDao.delete(customer);
		customerDao.closeCurrentSessionwithTransaction();
	}

	public List<Customer> findAll() {
		
		ArrayList<Customer> customersList = null;
		
		customerDao.openCurrentSession();
		List<CustomerBean> customers = customerDao.findAll();
		customerDao.closeCurrentSession();
		
		if(!customers.isEmpty()) {
			customersList = new ArrayList<Customer>();
			for(CustomerBean customer : customers) {
				customersList.add(customerDao.toEntity(customer));
			}
		}
		return customersList;
	}

	public void deleteAll() {
		customerDao.openCurrentSessionwithTransaction();
		customerDao.deleteAll();
		customerDao.closeCurrentSessionwithTransaction();
	}

	public CustomerDaoImpl getCustomerDao() {
		return customerDao;
	}

	public void setCustomerDao(CustomerDaoImpl customerDao) {
		this.customerDao = customerDao;
	}
	
	
}
