package fr.efrei.wish.business.services;

import java.util.ArrayList;
import java.util.List;

import fr.efrei.wish.business.entities.Comment;
import fr.efrei.wish.business.entities.repository.CommentBean;
import fr.efrei.wish.business.daos.CommentDaoImpl;

public class CommentService implements ServiceInterface<CommentBean, Comment> {

	private CommentDaoImpl commentDao;
	
	public void persist(Comment entity) {
		if(entity != null) {
			commentDao.openCurrentSessionwithTransaction();
			commentDao.persist(commentDao.toDto(entity));
			commentDao.closeCurrentSessionwithTransaction();
		}
	}

	public void update(Comment entity) {
		if(entity != null) {
			commentDao.openCurrentSessionwithTransaction();
			commentDao.update(commentDao.toDto(entity));
			commentDao.closeCurrentSessionwithTransaction();
		}
	}

	public Comment findById(String id) {
		commentDao.openCurrentSession();
		CommentBean comment = commentDao.findById(id);
		commentDao.closeCurrentSession();
		
		if(comment != null) {
			return commentDao.toEntity(comment);
		}
		return null;
	}

	public void delete(String id) {
		commentDao.openCurrentSessionwithTransaction();
		CommentBean comment = commentDao.findById(id);
		commentDao.delete(comment);
		commentDao.closeCurrentSessionwithTransaction();
	}

	public List<Comment> findAll() {
		ArrayList<Comment> commentsList = null;
		commentDao.openCurrentSession();
		List<CommentBean> comments = commentDao.findAll();
		commentDao.closeCurrentSession();
		
		if(comments != null && !comments.isEmpty()) {
			commentsList = new ArrayList<Comment>();
			for(CommentBean comment: comments) {
				commentsList.add(commentDao.toEntity(comment));
			}
		}
		return commentsList;
	}

	public void deleteAll() {
		commentDao.openCurrentSessionwithTransaction();
		commentDao.deleteAll();
		commentDao.closeCurrentSessionwithTransaction();
	}

	public CommentDaoImpl getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDaoImpl commentDao) {
		this.commentDao = commentDao;
	}
	
	

}
