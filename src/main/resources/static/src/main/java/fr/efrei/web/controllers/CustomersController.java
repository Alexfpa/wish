package fr.efrei.web.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.services.CustomerService;

@Controller
public class CustomersController {

	private CustomerService customerService;

	@GetMapping("/")
	public String getCustomer(Model model, @RequestParam(defaultValue = "0") int page) {
		model.addAttribute("data", customerService.findAll());
		return "index";
	}

	@PostMapping("/saveCustomer")
	public String saveCustomer(Customer customer) {
		customerService.persist(customer);

		return "redirect:/";
	}

	@PostMapping("/addCustomer")
	public String addCustomer(Customer customer, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-customer";
		}

		customerService.persist(customer);
		model.addAttribute("customer", customerService.findAll());
		return "index";
	}

	@PostMapping("/delete")
	public String deleteCustomer(String id) {
		customerService.delete(id);

		return "redirect:/";
	}

	@PostMapping("/findById")
	@ResponseBody
	public String findCustomer(String id) {
		customerService.findById(id);

		return "redirect:/";
	}
}
