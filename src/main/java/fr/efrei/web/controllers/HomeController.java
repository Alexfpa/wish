package fr.efrei.web.controllers;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.services.CustomerService;

@Controller
public class HomeController {
	
	private CustomerService customerService;
	
	public HomeController() {
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("businessServices.xml");
		
		customerService = (CustomerService) beanFactory.getBean("customerService");
	}
	
	@GetMapping("/")
	public String showPage(Model model) {
		Customer customer = customerService.findById("1");
		model.addAttribute("customer", customer);
		return "index";
	}
	
	@GetMapping("/signIn")
	public String signIn(Model model) {
		
		return "signIn";
	}
	
	
	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
}
