package fr.efrei.web.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.services.CustomerService;

@Controller
public class CustomersController {

	private CustomerService customerService;

	@PostMapping("/register")
	public String saveCustomer(Customer customer) {
		customerService.persist(customer);
		return "redirect:/";
	}

	@PostMapping("/addCustomer")
	public String addCustomer(Customer customer, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-customer";
		}

		customerService.persist(customer);
		model.addAttribute("customer", customerService.findAll());
		return "index";
	}

	@PostMapping("/delete")
	public String deleteCustomer(String id) {
		customerService.delete(id);

		return "redirect:/";
	}

	@PostMapping("/findById")
	@ResponseBody
	public String findCustomer(String id) {
		customerService.findById(id);

		return "redirect:/";
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	
}
