package fr.efrei.wish;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.efrei.web.controllers.CustomersController;
import fr.efrei.web.controllers.HomeController;
import fr.efrei.wish.business.services.CustomerService;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages= {"fr.efrei.web.controllers"})
public class WishApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(WishApplication.class, args);
	}
	
}
