package fr.efrei.wish.business.entities.repository;

import java.io.Serializable;

public class CommentBean implements Bean,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String text;

	private int productId;

	public CommentBean(String text) {
		this.text = text;
	}
	
	protected void initialize() {
	}
	
	public CommentBean() {
		this.initialize();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "CommentBean [id=" + id + ", text=" + text + ", productId=" + productId + "]";
	}

}
