package fr.efrei.wish.business.entities;


public class Comment implements Entity{

	private Integer id;
	
	private String text;
	
	private Integer productId;
	
	public Comment() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	
	
}
