package fr.efrei.wish.business.entities.repository;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order")
public class OrderBean implements Bean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "date")
	private Calendar date;

	@Column(name = "customer_id")
	private int customerId;

	public OrderBean(Calendar date, Integer customerId) {
		this.date = date;
		this.customerId = customerId;
	}
	
	protected void initialize() {
	}
	
	public OrderBean() {
		this.initialize();
	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", customerId=" + customerId + "]";
	}

}
