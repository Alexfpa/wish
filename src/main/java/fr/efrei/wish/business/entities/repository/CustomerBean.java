package fr.efrei.wish.business.entities.repository;

import java.io.Serializable;
import java.util.Date;

public class CustomerBean implements Bean,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String firstName;

	private String lastName;

	private Date birthDate;

	private int addressNumber;

	private String streetAddress;

	private String city;

	private int zipCode;

	private Date customerSince;

	public CustomerBean(String firstName, String lastName, Date birthDate, int addressNumber, String streetAddress,
			String city, int zipCode, Date customerSince) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.addressNumber = addressNumber;
		this.streetAddress = streetAddress;
		this.city = city;
		this.zipCode = zipCode;
		this.customerSince = customerSince;
	}
	
	protected void initialize() {
	}
	
	public CustomerBean() {
		this.initialize();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public int getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(int addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public Date getCustomerSince() {
		return customerSince;
	}

	public void setCustomerSince(Date customerSince) {
		this.customerSince = customerSince;
	}

	@Override
	public String toString() {
		return "CustomerBean [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", birthDate="
				+ birthDate + ", addressNumber=" + addressNumber + ", streetAddress=" + streetAddress + ", city=" + city
				+ ", zipCode=" + zipCode + ", customerSince=" + customerSince + "]";
	}

}
