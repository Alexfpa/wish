package fr.efrei.wish.business.daos;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.efrei.wish.business.entities.Comment;
import fr.efrei.wish.business.entities.repository.CommentBean;

public class CommentDaoImpl extends AbstractEntityDao implements EntityDao<CommentBean, Comment, String> {
	
	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
//		Configuration configuration = new Configuration().configure();
//		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
//				.applySettings(configuration.getProperties());
//		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//		return sessionFactory;
		
		try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();
            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (HibernateException he) {
            System.out.println("Session Factory creation failure");
            throw he;
        }
		
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(CommentBean entity) {
		getCurrentSession().save(entity);

	}

	public void update(CommentBean entity) {
		getCurrentSession().update(entity);

	}

	public CommentBean findById(String id) {
		CommentBean comment = (CommentBean) getCurrentSession().get(CommentBean.class, id);
		return comment;
	}

	public void delete(CommentBean entity) {
		getCurrentSession().delete(entity);
	}

	public List<CommentBean> findAll() {
		List<CommentBean> comments = (List<CommentBean>) getCurrentSession().createQuery("from Comment").list();
		return comments;

	}

	public void deleteAll() {
		List<CommentBean> entityList = findAll();
		for (CommentBean entity : entityList) {
			delete(entity);
		}

	}

	public Comment toEntity(CommentBean bean) {
		Comment comment = null;

		if (bean != null) {
			comment = new Comment();
			comment.setId(bean.getId());
			comment.setText(bean.getText());
			comment.setProductId(bean.getProductId());
		}

		return comment;
	}

	public CommentBean toDto(Comment entity) {
		CommentBean commentBean = null;

		if (entity != null) {
			commentBean = new CommentBean(entity.getText());
			commentBean.setId(entity.getId());
			commentBean.setProductId(entity.getProductId());
		}

		return commentBean;
	}

}
