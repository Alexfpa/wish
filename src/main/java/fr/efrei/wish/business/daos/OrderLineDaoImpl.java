package fr.efrei.wish.business.daos;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.efrei.wish.business.entities.OrderLine;
import fr.efrei.wish.business.entities.repository.OrderLineBean;

public class OrderLineDaoImpl extends AbstractEntityDao implements EntityDao<OrderLineBean, OrderLine, String> {
	
	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
//		Configuration configuration = new Configuration().configure();
//		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
//				.applySettings(configuration.getProperties());
//		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//		return sessionFactory;
		
		try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();
            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (HibernateException he) {
            System.out.println("Session Factory creation failure");
            throw he;
        }
		
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(OrderLineBean entity) {
		getCurrentSession().save(entity);

	}

	public void update(OrderLineBean entity) {
		getCurrentSession().update(entity);

	}

	public OrderLineBean findById(String id) {
		OrderLineBean orderLine = (OrderLineBean) getCurrentSession().get(OrderLineBean.class, id);
		return orderLine;
	}

	public void delete(OrderLineBean entity) {
		getCurrentSession().delete(entity);
	}

	public List<OrderLineBean> findAll() {
		List<OrderLineBean> orderLines = (List<OrderLineBean>) getCurrentSession().createQuery("from Customer").list();
		return orderLines;

	}

	public void deleteAll() {
		List<OrderLineBean> entityList = findAll();
		for (OrderLineBean entity : entityList) {
			delete(entity);
		}

	}

	public OrderLine toEntity(OrderLineBean bean) {
		OrderLine orderLine = null;
		
		if(bean != null) {
			orderLine = new OrderLine();
			orderLine.setProduct(null);
			orderLine.setAmount(bean.getAmount());
			orderLine.setPurchasePrice(new BigDecimal(bean.getPurchasePrice()));
			orderLine.setId(bean.getId());
			orderLine.setOrder(null);
		}
		return orderLine;
	}

	public OrderLineBean toDto(OrderLine entity) {
		OrderLineBean orderLineBean = null;
		
		if(entity != null) {
			orderLineBean = new OrderLineBean(entity.getAmount(), entity.getPurchasePrice().doubleValue());
			orderLineBean.setId(entity.getId());
			orderLineBean.setOrderId(entity.getOrder().getId());
			orderLineBean.setProductId(entity.getProduct().getId());
		}
		return orderLineBean;
	}
}
