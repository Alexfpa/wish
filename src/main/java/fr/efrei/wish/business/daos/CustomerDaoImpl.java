package fr.efrei.wish.business.daos;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.entities.repository.CustomerBean;

public class CustomerDaoImpl extends AbstractEntityDao implements EntityDao<CustomerBean, Customer, String> {
	
	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
//		Configuration configuration = new Configuration().configure();
//		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
//				.applySettings(configuration.getProperties());
//		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//		return sessionFactory;
		
		try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();
            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (HibernateException he) {
            System.out.println("Session Factory creation failure");
            throw he;
        }
		
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(CustomerBean entity) {
		getCurrentSession().save(entity);

	}

	public void update(CustomerBean entity) {
		getCurrentSession().update(entity);

	}

	public CustomerBean findById(String id) {
		CustomerBean customer = (CustomerBean) getCurrentSession().get(CustomerBean.class, Integer.parseInt(id));
		return customer;
	}

	public void delete(CustomerBean entity) {
		getCurrentSession().delete(entity);
	}

	public List<CustomerBean> findAll() {
		List<CustomerBean> customers = (List<CustomerBean>) getCurrentSession().createQuery("from CustomerBean").list();
		return customers;
	}

	public void deleteAll() {
		List<CustomerBean> entityList = findAll();
		for (CustomerBean entity : entityList) {
			delete(entity);
		}

	}

	public Customer toEntity(CustomerBean bean) {
		Customer customer = null;

		if (bean != null) {
			customer = new Customer(bean.getFirstName(), bean.getLastName());
			customer.setBirthDate(bean.getBirthDate());
			customer.setAddressNumber(bean.getAddressNumber());
			customer.setCity(bean.getCity());
			customer.setId(bean.getId());
			customer.setStreetAddress(bean.getStreetAddress());
			customer.setZipCode(bean.getZipCode());
			customer.setCustomerSince(bean.getCustomerSince());
		}

		return customer;
	}

	public CustomerBean toDto(Customer entity) {
		CustomerBean customerBean = null;

		if (entity != null) {
			customerBean = new CustomerBean(entity.getFirstName(), entity.getLastName(), entity.getBirthDate(),
					entity.getAddressNumber(), entity.getStreetAddress(), entity.getCity(), entity.getZipCode(),
					entity.getCustomerSince());
		}

		return customerBean;
	}

}
