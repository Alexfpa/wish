package fr.efrei.wish.business.daos;

import java.io.Serializable;
import java.util.List;

import fr.efrei.wish.business.entities.Entity;
import fr.efrei.wish.business.entities.repository.Bean;

public interface EntityDao<Bean, Entity, Id extends Serializable> {

	public void persist(Bean entity);

	public void update(Bean entity);

	public Bean findById(Id id);

	public void delete(Bean entity);

	public List<Bean> findAll();

	public void deleteAll();

	public Entity toEntity(Bean bean);

	public Bean toDto(Entity entity);

}
