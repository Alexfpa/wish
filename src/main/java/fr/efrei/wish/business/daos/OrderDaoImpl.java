package fr.efrei.wish.business.daos;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.efrei.wish.business.entities.Order;
import fr.efrei.wish.business.entities.repository.OrderBean;

public class OrderDaoImpl extends AbstractEntityDao implements EntityDao<OrderBean, Order, String> {
	
	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
//		Configuration configuration = new Configuration().configure();
//		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
//				.applySettings(configuration.getProperties());
//		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//		return sessionFactory;
		
		try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();
            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (HibernateException he) {
            System.out.println("Session Factory creation failure");
            throw he;
        }
		
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(OrderBean entity) {
		getCurrentSession().save(entity);

	}

	public void update(OrderBean entity) {
		getCurrentSession().update(entity);

	}

	public OrderBean findById(String id) {
		OrderBean order = (OrderBean) getCurrentSession().get(OrderBean.class, id);
		return order;
	}

	public void delete(OrderBean entity) {
		getCurrentSession().delete(entity);
	}

	public List<OrderBean> findAll() {
		List<OrderBean> orders = (List<OrderBean>) getCurrentSession().createQuery("from Customer").list();
		return orders;

	}

	public void deleteAll() {
		List<OrderBean> entityList = findAll();
		for (OrderBean entity : entityList) {
			delete(entity);
		}

	}

	public Order toEntity(OrderBean bean) {
		Order order = null;
		if(bean != null) {
			order = new Order();
			order.setId(bean.getId());
			order.setDate(bean.getDate());
			order.setCustomer(null);
			order.setOrderLines(null);
		}
		return order;
	}

	public OrderBean toDto(Order entity) {
		OrderBean orderBean = null;
		if (entity != null) {
			if (entity.getCustomer() != null) {
				orderBean = new OrderBean(entity.getDate(), entity.getCustomer().getId());
				orderBean.setId(entity.getId());
			}
		}
		return orderBean;

	}
}
