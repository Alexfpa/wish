package fr.efrei.wish.business.services;

import java.util.ArrayList;
import java.util.List;

import fr.efrei.wish.business.entities.Customer;
import fr.efrei.wish.business.entities.Order;
import fr.efrei.wish.business.entities.OrderLine;
import fr.efrei.wish.business.entities.repository.CustomerBean;
import fr.efrei.wish.business.entities.repository.OrderBean;
import fr.efrei.wish.business.entities.repository.OrderLineBean;
import fr.efrei.wish.business.entities.repository.ProductBean;
import fr.efrei.wish.business.daos.CustomerDaoImpl;
import fr.efrei.wish.business.daos.OrderDaoImpl;
import fr.efrei.wish.business.daos.OrderLineDaoImpl;
import fr.efrei.wish.business.daos.ProductDaoImpl;

public class OrderService implements ServiceInterface<OrderBean, Order> {

	private OrderDaoImpl orderDao;
	private OrderLineDaoImpl orderLineDao;
	private CustomerDaoImpl customerDao;
	private ProductDaoImpl productDao;

	public void persist(Order order) {
		if (order != null) {
			OrderBean orderBean = orderDao.toDto(order);
			orderDao.openCurrentSessionwithTransaction();
			orderDao.persist(orderBean);
			orderDao.closeCurrentSessionwithTransaction();
		}
	}

	public void update(Order order) {
		if(order!= null) {
			OrderBean orderBean = orderDao.toDto(order);
			orderDao.openCurrentSessionwithTransaction();
			orderDao.update(orderBean);
			orderDao.closeCurrentSessionwithTransaction();
		}
	}

	public Order findById(String id) {
		Order order = null;
		CustomerBean customerBean = null;
		Customer customer = null;
		ArrayList<OrderLine> orderLineList = new ArrayList<OrderLine>();
		
		orderDao.openCurrentSession();
		OrderBean orderBean = orderDao.findById(id);
		
		List<OrderLineBean> orderLineBeanList = orderLineDao.findAll();
		
		if(orderBean != null) {
			order = orderDao.toEntity(orderBean);
			if(order != null) {
				customerBean = customerDao.findById(""+order.getId());
				if(customerBean != null) {
					customer = customerDao.toEntity(customerBean);
					order.setCustomer(customer);
				}
				
				if(orderLineBeanList != null && !orderLineBeanList.isEmpty()) {
					for(OrderLineBean orderLineBean : orderLineBeanList) {
						if(orderLineBean.getOrderId() == order.getId()) {
							
							productDao.openCurrentSession();
							ProductBean productBean = productDao.findById(""+orderLineBean.getProductId());
							productDao.closeCurrentSession();
							
							orderDao.openCurrentSession();
							OrderBean orderBean2 = orderDao.findById(""+orderLineBean.getOrderId());
							orderDao.closeCurrentSession();
							
							if(orderLineBean != null && productBean != null && orderBean2 != null) {
								OrderLine orderLine = orderLineDao.toEntity(orderLineBean);
								orderLine.setProduct(productDao.toEntity(productBean));
								orderLine.setOrder(orderDao.toEntity(orderBean));
								orderLineList.add(orderLine);
							}
						}
					}
				}
				order.setOrderLines(orderLineList);	
			}	
		}
		return order;
	}

	public void delete(String id) {
		orderDao.openCurrentSessionwithTransaction();
		OrderBean order = orderDao.findById(id);
		orderDao.delete(order);
		orderDao.closeCurrentSessionwithTransaction();
	}

	public List<Order> findAll() {
		List<Order> orders = new ArrayList<Order>();
		orderDao.openCurrentSession();
		List<OrderBean> orderBeans = orderDao.findAll();
		orderDao.closeCurrentSession();
		
		orderLineDao.openCurrentSession();
		List<OrderLineBean> orderLineBeanList = orderLineDao.findAll();
		orderLineDao.closeCurrentSession();
		
		for(OrderBean orderBean : orderBeans) {
			Order order = orderDao.toEntity(orderBean);
			ArrayList<OrderLine> orderLineList = new ArrayList<OrderLine>();
			if(order != null) {
				CustomerBean customerBean = customerDao.findById(""+order.getId());
				if(customerBean != null) {
					Customer customer = customerDao.toEntity(customerBean);
					order.setCustomer(customer);
				}
				
				if(orderLineBeanList != null && !orderLineBeanList.isEmpty()) {
					for(OrderLineBean orderLineBean : orderLineBeanList) {
						if(orderLineBean.getOrderId() == order.getId()) {
							
							productDao.openCurrentSession();
							ProductBean productBean = productDao.findById(""+orderLineBean.getProductId());
							productDao.closeCurrentSession();
							
							orderDao.openCurrentSession();
							OrderBean orderBean2 = orderDao.findById(""+orderLineBean.getOrderId());
							orderDao.closeCurrentSession();
							
							if(orderLineBean != null && productBean != null && orderBean2 != null) {
								OrderLine orderLine = orderLineDao.toEntity(orderLineBean);
								orderLine.setProduct(productDao.toEntity(productBean));
								orderLine.setOrder(orderDao.toEntity(orderBean));
								orderLineList.add(orderLine);
							}
						}
					}
				}
				order.setOrderLines(orderLineList);	
			}	
			orders.add(order);
		}
		return orders;
	}
	
	public void deleteAll() {
		orderDao.openCurrentSessionwithTransaction();
		orderDao.deleteAll();
		orderDao.closeCurrentSessionwithTransaction();
	}

	public OrderDaoImpl getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDaoImpl orderDao) {
		this.orderDao = orderDao;
	}

	public OrderLineDaoImpl getOrderLineDao() {
		return orderLineDao;
	}

	public void setOrderLineDao(OrderLineDaoImpl orderLineDao) {
		this.orderLineDao = orderLineDao;
	}

	public CustomerDaoImpl getCustomerDao() {
		return customerDao;
	}

	public void setCustomerDao(CustomerDaoImpl customerDao) {
		this.customerDao = customerDao;
	}

	public ProductDaoImpl getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDaoImpl productDao) {
		this.productDao = productDao;
	}

}
