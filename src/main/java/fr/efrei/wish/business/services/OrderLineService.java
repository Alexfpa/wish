package fr.efrei.wish.business.services;

import java.util.ArrayList;
import java.util.List;

import fr.efrei.wish.business.entities.OrderLine;
import fr.efrei.wish.business.entities.repository.OrderBean;
import fr.efrei.wish.business.entities.repository.OrderLineBean;
import fr.efrei.wish.business.entities.repository.ProductBean;
import fr.efrei.wish.business.daos.OrderDaoImpl;
import fr.efrei.wish.business.daos.OrderLineDaoImpl;
import fr.efrei.wish.business.daos.ProductDaoImpl;

public class OrderLineService implements ServiceInterface<OrderLineBean, OrderLine> {

	private OrderLineDaoImpl orderLineDao;
	private ProductDaoImpl productDao;
	private OrderDaoImpl orderDao;

	public OrderLineService() {
		productDao = new ProductDaoImpl();
		orderLineDao = new OrderLineDaoImpl();
		orderDao = new OrderDaoImpl();
	}

	public void persist(OrderLine orderLine) {
		OrderLineBean orderLineBean = null;
		if (orderLine != null) {
			orderLineBean = orderLineDao.toDto(orderLine);
			orderLineDao.openCurrentSessionwithTransaction();
			orderLineDao.persist(orderLineBean);
			orderLineDao.closeCurrentSessionwithTransaction();
		}

	}

	public void update(OrderLine orderLine) {
		OrderLineBean orderLineBean = null;
		if (orderLine != null) {
			orderLineBean = orderLineDao.toDto(orderLine);
			orderLineDao.openCurrentSessionwithTransaction();
			orderLineDao.update(orderLineBean);
			orderLineDao.closeCurrentSessionwithTransaction();
		}
	}

	public OrderLine findById(String id) {
		OrderLine orderLine = null;

		orderLineDao.openCurrentSession();
		OrderLineBean orderLineBean = orderLineDao.findById(id);
		orderLineDao.closeCurrentSession();
		
		productDao.openCurrentSession();
		ProductBean productBean = productDao.findById(""+orderLineBean.getProductId());
		productDao.closeCurrentSession();
		
		orderDao.openCurrentSession();
		OrderBean orderBean = orderDao.findById(""+orderLineBean.getOrderId());
		orderDao.closeCurrentSession();
		
		if(orderLineBean != null && productBean != null && orderBean != null) {
			orderLine = orderLineDao.toEntity(orderLineBean);
			orderLine.setProduct(productDao.toEntity(productBean));
			orderLine.setOrder(orderDao.toEntity(orderBean));
		}

		return orderLine;
	}

	public void delete(String id) {
		productDao.openCurrentSessionwithTransaction();
		ProductBean product = productDao.findById(id);
		productDao.delete(product);
		productDao.closeCurrentSessionwithTransaction();
	}

	public List<OrderLine> findAll() {
		ArrayList<OrderLine> orderLineList = null;
		
		orderLineDao.openCurrentSession();
		List<OrderLineBean> orderLineBeanList = orderLineDao.findAll();
		orderLineDao.closeCurrentSession();
		
		for(OrderLineBean orderLineBean : orderLineBeanList) {
			
			orderDao.openCurrentSession();
			OrderBean orderBean = orderDao.findById(""+orderLineBean.getOrderId());
			orderDao.closeCurrentSession();
			
			productDao.openCurrentSession();
			ProductBean productBean = productDao.findById(""+orderLineBean.getProductId());
			productDao.closeCurrentSession();
			
			if(orderBean != null && productBean != null) {
				OrderLine orderLine = orderLineDao.toEntity(orderLineBean);
				orderLine.setProduct(productDao.toEntity(productBean));
				orderLine.setOrder(orderDao.toEntity(orderBean));
				if(orderLineList == null) {
					orderLineList = new ArrayList<OrderLine>();
				}
				orderLineList.add(orderLine);
			}
		}
		return orderLineList;
	}

	public void deleteAll() {
		productDao.openCurrentSessionwithTransaction();
		productDao.deleteAll();
		productDao.closeCurrentSessionwithTransaction();
	}

	public OrderLineDaoImpl getOrderLineDao() {
		return orderLineDao;
	}

	public void setOrderLineDao(OrderLineDaoImpl orderLineDao) {
		this.orderLineDao = orderLineDao;
	}

	public ProductDaoImpl getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDaoImpl productDao) {
		this.productDao = productDao;
	}

	public OrderDaoImpl getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDaoImpl orderDao) {
		this.orderDao = orderDao;
	}

	
	
}
